# WatchDogsFont.com / hacked.watch/dogs
> Cool website about a font. http://www.dafont.com/hacked.font


## Resources used
- Pixel Operator font : http://www.dafont.com/pixel-operator.font
- Ubisoft Dedsec design kits : http://watchdogs.ubisoft.com/watchdogs/en-US/news/152-259890-16/fr-FR
- VR engine : https://aframe.io/

## Licence
CC-BY https://creativecommons.org/licenses/by/4.0/
